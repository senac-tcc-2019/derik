# Travelers Client
 O projeto Travelers trata-se de uma rede social voltada para pessoas que gostam de viajar e/ou conhecer novos lugares, e tem como objetivo trazer,
através da intereação de usuários, informações mais qualificadas sobre diferentes lugares para se viajar.

# Das Tecnologias utilizadas no Front-end do projeto
  O presente projeto utiliza em seu front-end a tecnologia React. Ao final deste readme estará disponível o link do projeto, já em funcionamento parcial para fins de teste, contudo, caso tenha-se
  vontade de subir o projeto em sua própria máquina, primeiramente será necessário ter a API previamente instalada(instruções [Aqui](https://gitlab.com/senac-tcc-2019/derik/blob/master/README.md)),
  e as seguintes tecnologias instaladas:
   - Yarn ou Npm
   - React

 Para uma instalação simples e rápida dos programas necessários para rodar a front-end, indica-se seguir o passo a passo do site [reactjs.org](https://reactjs.org/docs/getting-started.html).
 
 Para executar o projeto local basta seguir os passos abaixo: 
 Após ter instalado o necessário para subir a API, prepare a mesma, criando o banco:
  - rails db:create
  - rails db:migrate

  Suba o servidor da API executando o seguinte comando no console:
  - rails s -p 3001

  Suba o front-end através do react, digitando em outra aba do console:
   - yarn start 
   
ou

   - npm start

  Aguarde o projeto abrir automaticamente no seu browser padrão e então comece a fazer seus testes.
  
# Link do projeto em funcionamento parcial
 O projeto já está disponível para fins testes no link abaixo:
 - https://socialtravelers.herokuapp.com/

**Obs.:**
Ao criar um novo usuário, será necessário preencher o País, Estado e Cidade, porém, os mesmos ainda não estão totalmente prontos, por tanto, disponibilizamos um `id` específico para teste, basta
preencher os campos citados com o número `2`.

