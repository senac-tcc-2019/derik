import React, { Component } from 'react'
import { Row, Col, Card } from 'react-materialize';
import { LocalForm, Control } from 'react-redux-form';

class SignUp extends Component {

  render() {
    return (
      <Row>
        <Col s={12} offset="2">
          <Card>
            <LocalForm onSubmit={(values) => this.props.handleSubmit(values) }>
              <Control.text model=".name"
                name="name"
                type="text"
                placeholder="Nome"
              />
              <Control.text model=".last_name"
                name="lastname"
                type="text"
                placeholder="Último Nome"
              />
              <Control.text model=".username"
                name="username"
                type="text"
                placeholder="Username"
              />
              <Control.text model=".email"
                name="email"
                type="text"
                placeholder="E-mail"
              />
              <Control.text model=".password"
                name="password"
                type="password"
                placeholder="Senha"
              />
            <Control.select className="browser-default" style={{marginBottom: '5px'}} model=".country_id" onChange={(event) => this.props.getStates(event)} >
                <option selected value='' > Selecione o País </option>
                {
                  this.props.countries.length > 0 &&
                  this.props.countries.map((country, index) => {
                    return(
                      <option key={index} name="country" value={country.id}> {country.name} </option>
                    )
                  })
                }
              </Control.select>
              <Control.select className="browser-default" style={{marginBottom: '5px'}} model=".state_id" onChange={(event) => this.props.getCities(event)} >
                <option selected value='' > Selecione o Estado </option>
                {
                  this.props.states.length > 0 &&
                  this.props.states.map((state, index) => {
                    return(
                      <option key={index} name="state" value={state.id}> {state.name} </option>
                    )
                  })
                }
              </Control.select>
              <Control.select className="browser-default" style={{marginBottom: '5px'}} model=".city_id">
                <option selected value='' > Selecione a Cidade </option>
                {
                  this.props.cities.length > 0 &&
                  this.props.cities.map((city, index) => {
                    return(
                      <option key={index} name="city" value={city.id}> {city.name} </option>
                    )
                  })
                }
              </Control.select>
              <div className="text-right">
                <button type="submit" className="blue btn grey darken-2"> Registrar-se </button>
              </div>
            </LocalForm>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default SignUp;
