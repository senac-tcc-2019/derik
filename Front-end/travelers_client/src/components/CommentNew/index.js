import React from "react";
import { Row, Col, Card, TextInput } from 'react-materialize';

const CommentNew = (props) => {
  let postId = props.postId
  let setCommentsCount = props.setCommentsCount
  return (
    <Card>
      <Row>
        <Col s={12}>
          <TextInput icon="send" s={12} maxLength="200" label="No que você está pensando?" validate onKeyDown={(event) => props.postComment({event, postId, setCommentsCount})}/>
        </Col>
      </Row>
    </Card>
  )
}

export const CommentEdit = (props) => {
  let commentId = props.id
  let setShowComments = props.setShowComments
  return (
    <Card>
      <Row>
        <Col s={12}>
          <TextInput icon="send" s={12} maxLength="200" defaultValue={props.body} validate onKeyDown={(event) => props.putComment({event, commentId, setShowComments})}/>
        </Col>
      </Row>
    </Card>
  )
}

export default CommentNew;
