import React from "react";
import { Row, Col, Card, Icon, Dropdown, NavItem, Modal } from 'react-materialize';
import styled from 'styled-components';
import InvisibleButton from '../../components/common/InvisibleButton';
import fake_avatar from '../../images/avatar.png';
import { CommentEdit } from '../CommentNew';

const Avatar = styled.img`
 width: 100%`
;

const CommentUnit = (props) => (
  <Card>
    <Row>
      <Col s={6} m={2} offset="s3">
        <a href={`/user/${props.user.id}`}>
          <Avatar src={(props.user.photo && props.user.photo.url) ? props.user.photo.profile.url : fake_avatar } className="responsive-img circle" />
        </a>
      </Col>
      <Col s={12} m={10}>
        <Row>
          <Col s={10} m={10}>
            <b>{props.user.name} </b>
          </Col>
          <Col s={2} m={2} className="right-align">
            {
              props.user.id === props.current_user.id &&
              <Dropdown trigger={
                  <InvisibleButton>
                    <Icon>expand_more</Icon>
                  </InvisibleButton>
                }>
                <NavItem>
                  <Modal trigger={ <InvisibleButton className="grey-text text-darken-2" style={{ cursor: 'pointer' }}> Editar</InvisibleButton> }
                         actions={ <button className="waves-effect waves-green btn-flat modal-close">Cancelar</button> }
                    >
                    <h5 className="grey-text text-darken-2">Atualize seu comentário</h5>
                    <CommentEdit {...props} />
                  </Modal>
                </NavItem>
                <NavItem>
                  <span className="grey-text text-darken-2" onClick={() => props.deleteComment(props.id, props.setCommentsCount)} >Excluir</span>
                </NavItem>
              </Dropdown>
            }
          </Col>
        </Row>
        <Row>
          <Col s={12} m={12}>
            {props.body}
          </Col>
        </Row>
        <Row>
          <Col s={1} m={1}>
            <InvisibleButton>
              <Icon>thumb_up</Icon>
            </InvisibleButton>
          </Col>
        </Row>
      </Col>
    </Row>
  </Card>
);

export default CommentUnit;
