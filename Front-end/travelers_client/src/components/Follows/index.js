import React from "react";
import InvisibleButton from '../common/InvisibleButton';
import { Icon } from 'react-materialize';

export const UserFollow = (props) => (
  <InvisibleButton onClick={() => props.follow(props.user)}>
    <Icon small className="yellow-text text-darken-2">star_border</Icon>
  </InvisibleButton>
)

export const UserUnFollow = (props) => (
  <InvisibleButton onClick={() => props.unfollow(props.user)}>
    <Icon small className="yellow-text text-darken-2">star</Icon>
  </InvisibleButton>
)
