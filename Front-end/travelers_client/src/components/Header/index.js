import React from "react";
import { Navbar, NavItem, Row, Icon } from 'react-materialize';
import travelers_logo_name from '../../images/travelers_logo_name.png';
import styled from 'styled-components';

const NavbarBlue = styled(Navbar)`
  background-color: #cfe2f3;
`;

const IconUser = styled(Icon)`
  font-size: 40px !important;
`;

const Logo = styled.img`
  width: 150px;
  margin-left: 30px;
  transform: rotate(4deg);
`;

const Header = (props) => (
  <Row>
    <NavbarBlue brand={<a href="/timeline "><Logo src={travelers_logo_name} className="responsive-img"/></a>} alignLinks="right">
      <NavItem href="/preferences">
        Preferências
      </NavItem>
      <NavItem onClick={() => props.logOut()}>
        Logout
      </NavItem>
      <NavItem href="/user/x/edit">
        <IconUser>account_circle</IconUser>
      </NavItem>
    </NavbarBlue>
  </Row>
);

export default Header;
