import React, { Fragment } from "react";
import { Row, Col, Button, Icon } from 'react-materialize';
import { LocalForm, Control } from 'react-redux-form';

const ItemPreferences = (props) => (
  <Fragment>
    <Row>
      <LocalForm onSubmit={(values) => props.handleSubmit(values) }>
        <Col s={10}>
          <Control.select className="browser-default" model=".type_id">
            <option selected value='' > Selecione o tipo de Local </option>
              {
                props.types.length > 0 &&
                props.types.map((type, index) => {
                  return(
                    <option key={index} name="type" value={type.id}> {type.name} </option>
                  )
                })
              }
          </Control.select>
        </Col>
        <Col s={2} className="offset" >
          <Button floating small className="green" waves="light" icon={<Icon> add_circle </Icon>}/>
        </Col>
      </LocalForm>
    </Row>
    <Row>
      <LocalForm onSubmit={(values) => props.handleSubmit(values) }>
        <Col s={10}>
          <Control.select className="browser-default" model=".country_id" onChange={(event) => props.getStates(event)}>
            <option selected value='' > Selecione o País </option>
              {
                props.countries.length > 0 &&
                props.countries.map((country, index) => {
                  return(
                    <option key={index} name="country" value={country.id}> {country.name} </option>
                  )
                })
              }
          </Control.select>
        </Col>
        <Col s={2} className="offset" >
          <Button floating small className="green" waves="light" icon={<Icon> add_circle </Icon>}/>
        </Col>
      </LocalForm>
    </Row>
    <Row>
      <LocalForm onSubmit={(values) => props.handleSubmit(values) }>
        <Col s={10}>
          <Control.select className="browser-default"  model=".state_id" onChange={(event) => props.getCities(event)}>
            <option selected value='' > Selecione o Estado </option>
              {
                props.states.length > 0 &&
                props.states.map((state, index) => {
                  return(
                    <option key={index} name="state" value={state.id}> {state.name} </option>
                  )
                })
              }
          </Control.select>
        </Col>
        <Col s={2} className="offset" >
          <Button floating small className="green" waves="light" icon={<Icon> add_circle </Icon>}/>
        </Col>
      </LocalForm>
    </Row>
    <Row>
      <LocalForm onSubmit={(values) => props.handleSubmit(values) }>
        <Col s={10}>
          <Control.select className="browser-default" model=".city_id" >
            <option selected value='' > Selecione a Cidade </option>
              {
                props.cities.length > 0 &&
                props.cities.map((city, index) => {
                  return(
                    <option key={index} name="city" value={city.id}> {city.name} </option>
                  )
                })
              }
          </Control.select>
        </Col>
        <Col s={2} className="offset" >
          <Button floating small className="green" waves="light" icon={<Icon> add_circle </Icon>}/>
        </Col>
      </LocalForm>
    </Row>
  </Fragment>
);

export default ItemPreferences;
