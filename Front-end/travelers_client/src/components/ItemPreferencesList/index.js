import React from "react";
import { Icon, Button } from 'react-materialize';
import styled from 'styled-components';

const ButtonTd = styled.td`
  float: right;
`;

export const TypeList = (props) => {
  return (
    Object.values(props.items).map( item => {
      if(item.type) {
        return(
          <tr>
            <td> { item.type.name } </td>
            <ButtonTd>
              <Button floating small className="red" waves="light" icon={<Icon> remove_circle </Icon>} onClick={ () => props.delItemPreference(item.id) } />
            </ButtonTd>
          </tr>
        )
      }
      else { return null }
    })
  )
}

export const CountryList = (props) => {
  return (
    Object.values(props.items).map( item => {
      if(item.country) {
        return(
          <tr>
            <td> { item.country.name } </td>
            <ButtonTd>
              <Button floating small className="red" waves="light" icon={<Icon> remove_circle </Icon>} onClick={ () => props.delItemPreference(item.id) } />
            </ButtonTd>
          </tr>
        )
      }
      else { return null }
    })
  )
}

export const StateList = (props) => {
  return (
    Object.values(props.items).map( item => {
      if(item.state) {
        return(
          <tr>
            <td> { item.state.name } </td>
            <ButtonTd>
              <Button floating small className="red" waves="light" icon={<Icon> remove_circle </Icon>} onClick={ () => props.delItemPreference(item.id) } />
            </ButtonTd>
          </tr>
        )
      }
      else { return null }
    })
  )
}

export const CityList = (props) => {
  return (
    Object.values(props.items).map( item => {
      if(item.city) {
        return(
          <tr>
            <td> { item.city.name } </td>
            <ButtonTd>
              <Button floating small className="red" waves="light" icon={<Icon> remove_circle </Icon>} onClick={ () => props.delItemPreference(item.id) } />
            </ButtonTd>
          </tr>
        )
      }
      else { return null }
    })
  )
}
