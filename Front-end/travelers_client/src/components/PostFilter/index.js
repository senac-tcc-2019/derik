import React from "react";
import { Row, Col, Card, Button } from 'react-materialize';
import { LocalForm, Control } from 'react-redux-form';

const PostFilter = (props) => (
  <div>
    <Card>
      <LocalForm onSubmit={(values) => props.postFilter(values)} >
        <Row style={{ textAlign: 'center' }}>
          <h5 style={{ color: 'darkgrey', fontWeight: 'bold' }}>Filtro de Postagens</h5>
          <hr/>
        </Row>
        <Row>
          <Col s={12}>
            <Row>
              <Control.select className="browser-default" model=".type_id">
                <option selected value='' > Selecione o Tipo de Local </option>
                {
                  props.types.length > 0 &&
                  props.types.map((type, index) => {
                    return(
                      <option key={index} name="type" value={type.id}> {type.name} </option>
                    )
                  })
                }
              </Control.select>
            </Row>
            <Row>
              <Control.select className="browser-default" model=".country_id" onChange={(event) => props.getStates(event)}>
                <option selected value='' > Selecione o País </option>
                {
                  props.countries.length > 0 &&
                  props.countries.map((country, index) => {
                    return(
                      <option key={index} name="country" value={country.id}> {country.name} </option>
                    )
                  })
                }
              </Control.select>
            </Row>
            <Row>
              <Control.select className="browser-default" model=".state_id" onChange={(event) => props.getCities(event)}>
                <option selected value='' > Selecione o Estado </option>
                {
                  props.states.length > 0 &&
                  props.states.map((state, index) => {
                    return(
                      <option key={index} name="state" value={state.id}> {state.name} </option>
                    )
                  })
                }
              </Control.select>
            </Row>
            <Row>
              <Control.select className="browser-default" model=".city_id">
                <option selected value='' > Selecione a Cidade </option>
                {
                  props.cities.length > 0 &&
                  props.cities.map((city, index) => {
                    return(
                      <option key={index} name="city" value={city.id}> {city.name} </option>
                    )
                  })
                }
              </Control.select>
            </Row>
          </Col>
        </Row>
        <Row>
          <Button className="grey waves-effect waves-green btn-flat modal-close"> Filtrar </Button>
        </Row>
      </LocalForm>
    </Card>
  </div>
)

export default PostFilter;
