import React from "react";
import { Row, Col, Card, Modal, Button } from 'react-materialize';
import { LocalForm, Control } from 'react-redux-form';

const PostNew = (props) => (
  <Card>
    <Row style={{ textAlign: 'center' }}>
      <Modal trigger={ <button className="grey-text text-darken-2" style={{ cursor: 'pointer' }}> Criar Postagem</button> }
             actions={false}
      >
        <LocalForm onSubmit={(values) => props.postPost(values)} >
          <Row>
            <Col s={12}>
              <Col s={3} style={{ float: 'left' }}>
                <Control.select className="browser-default" model=".type_id">
                  <option selected value='' > Selecione o Tipo de Local </option>
                  {
                    props.types.length > 0 &&
                    props.types.map((type, index) => {
                      return(
                        <option key={index} name="type" value={type.id}> {type.name} </option>
                      )
                    })
                  }
                </Control.select>
              </Col>
              <Col s={3} style={{ float: 'left'}}>
                <Control.select className="browser-default" model=".country_id" onChange={(event) => props.getStates(event)}>
                  <option selected value='' > Selecione o País </option>
                  {
                    props.countries.length > 0 &&
                    props.countries.map((country, index) => {
                      return(
                        <option key={index} name="country" value={country.id}> {country.name} </option>
                      )
                    })
                  }
                </Control.select>
              </Col>
              <Col s={3} style={{ float: 'left' }}>
                <Control.select className="browser-default" model=".state_id" onChange={(event) => props.getCities(event)}>
                  <option selected value='' > Selecione o Estado </option>
                  {
                    props.states.length > 0 &&
                    props.states.map((state, index) => {
                      return(
                        <option key={index} name="state" value={state.id}> {state.name} </option>
                      )
                    })
                  }
                </Control.select>
              </Col>
              <Col s={3} style={{ float: 'left' }}>
                <Control.select className="browser-default" model=".city_id">
                  <option selected value='' > Selecione a Cidade </option>
                  {
                    props.cities.length > 0 &&
                    props.cities.map((city, index) => {
                      return(
                        <option key={index} name="city" value={city.id}> {city.name} </option>
                      )
                    })
                  }
                </Control.select>
              </Col>
            </Col>
          </Row>
          <Row>
            <Col s={12}>
              {
                !props.postImage.postImage &&
                <Card>
                    <div className="card-image">
                      <img src={props.postImage} alt="" />
                    </div>
                </Card>
              }
              <Control.file model=".photo"
                  onChange={(value) => props.encodeFile(value)}
              />
            </Col>
          </Row>
          <Row>
            <Col s={12}>
              <Control.textarea style={{ padding: '10px' }} s={8} name="body" model=".body" />
            </Col>
          </Row>
          <Col s={6} style={{ float: 'left', marginLeft: '.75rem' }}>
            <Button className="grey waves-effect waves-green btn-flat modal-close"> Postar </Button>
          </Col>
        </LocalForm>
        <Col s={6} style={{ float: 'right', marginRight: '.75rem' }}>
          <Button className="grey waves-effect waves-green btn-flat modal-close"> Cancelar </Button>
        </Col>
      </Modal>
    </Row>
  </Card>
)

export default PostNew;
