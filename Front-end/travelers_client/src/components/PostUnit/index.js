import React, { useState } from "react";
import { Row, Col, Card, Icon, Dropdown, NavItem, Modal } from 'react-materialize';
import styled from 'styled-components';
import InvisibleButton from '../../components/common/InvisibleButton';
import CommentButton from '../../components/common/CommentButton';
import fake_avatar from '../../images/avatar.png';
import CommentListContainer from '../../containers/CommentListContainer';

const Avatar = styled.img`
 width: 100%`
;

const PostUnit = (props) => {
  const [showComments, setShowComments] = useState(false);
  const [createComments, setCreateComments] = useState(false);
  const [commentsCount, setCommentsCount] = useState(props.comments.length)
  return (
    <Card>
      <Row>
        <Col s={6} m={2} offset="s3">
          <a href={`/user/${props.user.id}`}>
            <Avatar src={(props.user.photo && props.user.photo.url) ? props.user.photo.profile.url : fake_avatar } className="responsive-img circle" />
          </a>
        </Col>
        <Col s={12} m={10}>
          <Row>
            <Col s={10} m={10}>
              <b>{props.user.name} - {props.time}</b>
            </Col>
            <Col s={2} m={2} className="right-align">
              { props.current_user.id === props.user_id &&
                <Dropdown trigger={
                    <InvisibleButton>
                      <Icon>expand_more</Icon>
                    </InvisibleButton>
                  }>
                  <NavItem>
                    <span className="grey-text text-darken-2">Editar</span>
                  </NavItem>
                  <NavItem onClick={() => props.deletePost(props.id)}>
                    <span className="grey-text text-darken-2">Excluir</span>
                  </NavItem>
                </Dropdown>
              }
            </Col>
          </Row>
          <Row>
            <div className="card-image">
              <Col s={10} m={10}>
                <Modal trigger={<img src={props.photo.url} alt=""  style={{ cursor: "pointer"}} color="green" />} >
                  <div className="modal-content" style={{ textAlign: 'center' }}>
                    <Row>
                      <img src={props.photo.url} alt="" style={{ margin: 'auto' }}/>
                    </Row>
                  </div>
                </Modal>
                <i>{props.city.name} - {props.country.name} </i>
                <span style={{ float: 'right', fontSize: 'small', color: 'grey'}}> postado em: {props.post_date} </span>
              </Col>
            </div>
          </Row>
          <Row>
            <Col s={12} m={12}>
              {props.body}
            </Col>
          </Row>
          <Row>
            <Col s={1} m={1}>
              <InvisibleButton>
                <Icon>thumb_up</Icon>
              </InvisibleButton>
            </Col>
            <Col m={2}>
              <Col m={1}>
                <InvisibleButton>
                  <Icon>message</Icon>
                </InvisibleButton>
              </Col>
              <Col m={1} className="grey-text" style={{marginLeft: "5px"}} >
                {commentsCount}
              </Col>
            </Col>
            {
              commentsCount > 0 &&
              <Col m={4}>
                {
                  !showComments ?
                    <CommentButton onClick={() => { setShowComments(true); props.getCommentList(props.id)}} > Mostrar comentários </CommentButton>
                  :
                    <CommentButton onClick={() => setShowComments(false)} > Ocultar comentários </CommentButton>
                }
              </Col>
            }
            <Col m={4}>
              {
                !createComments ?
                  <CommentButton onClick={() => { setCreateComments(true); props.getCommentList(props.id)}} > Criar comentário </CommentButton>
                :
                  <CommentButton onClick={() => setCreateComments(false)} > Cancelar </CommentButton>
              }
            </Col>
          </Row>
        </Col>
      </Row>
      {
        createComments &&
        <CommentListContainer createComments={createComments} postId={props.id} setCommentsCount={setCommentsCount} />
      }
      {
        showComments &&
        <CommentListContainer postId={props.id} setShowComments={setShowComments} setCommentsCount={setCommentsCount} />
      }
    </Card>
  )
};

export default PostUnit;
