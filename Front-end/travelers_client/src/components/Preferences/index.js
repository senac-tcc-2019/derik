import React from "react";
import { Row, Card, Col } from 'react-materialize';
import { LocalForm, Control } from 'react-redux-form';
import HeaderContainer from '../../containers/HeaderContainer';
import ItemPreferencesContainer from '../../containers/ItemPreferencesContainer';
import { CountryList, StateList, CityList, TypeList } from '../ItemPreferencesList';

const Preferences = (props) => (
  <div>
    <HeaderContainer/>
    <Row>
      <Col s={12} m={6} className="offset-m3">
        <Card>
          <Row>
            <Col m={8} s={8}>
              <b className="grey-text text-darken-1">Preferências do Usuário</b>
            </Col>
          </Row>
          <Row>
            <Col m={6} >
              <b className="grey-text text-darken-1">Descrição</b>
              <Card>
                <div className="form">
                  <LocalForm onSubmit={(values) => props.updatePreference(values) } initialState={{ id: props.preference.id, description: props.preference.description }} >
                    <Control.textarea model=".description" name="description" />
                    <button type="submit" className="blue btn grey darken-2" >Atualizar</button>
                  </LocalForm>
                </div>
              </Card>
            </Col>
            <Col m={6} style={{ float: 'right' }} >
              <b className="grey-text text-darken-1">Lista de Preferências</b>
              <Card>
                <Row>
                  <b className="grey-text text-darken-1">Tipos de Locais Preferidos</b>
                  <Card>
                    <table>
                      <TypeList items={props.itemPreferences} delItemPreference={props.delItemPreference} />
                    </table>
                  </Card>
                </Row>
                <Row>
                  <b className="grey-text text-darken-1">Países Preferidos</b>
                  <Card>
                    <table>
                      <CountryList items={props.itemPreferences} delItemPreference={props.delItemPreference} />
                    </table>
                  </Card>
                </Row>
                <Row>
                  <b className="grey-text text-darken-1">Estados Preferidos</b>
                  <Card>
                    <table>
                      <StateList items={props.itemPreferences} delItemPreference={props.delItemPreference} />
                    </table>
                  </Card>
                </Row>
                <Row>
                  <b className="grey-text text-darken-1">Cidades Preferidas</b>
                  <Card>
                    <table>
                      <CityList items={props.itemPreferences} delItemPreference={props.delItemPreference} />
                    </table>
                  </Card>
                </Row>
              </Card>
            </Col>
            <Col m={6} >
              <b className="grey-text text-darken-1">Adicionar novas preferências</b>
              <Card>
                <ItemPreferencesContainer />
              </Card>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  </div>
);

export default Preferences;
