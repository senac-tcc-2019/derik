import React from "react";
import { Row, Col, Card } from 'react-materialize';
import HeaderContainer from '../../containers/HeaderContainer';
import { LocalForm, Control } from 'react-redux-form';
import styled from 'styled-components';

const ProfileImage = styled.img`
  margin-top: 10px;
  margin-bottom: 10px;
`;

const ProfileEdit = (props) => (
  <div>
    <HeaderContainer/>
    <Row>
      <Col s={12} m={6} className="offset-m3">
        <Card>
          <Row>
            <Col m={8} s={8} offset="">
              <b className="grey-text text-darken-1">Informações do Perfil</b>
            </Col>
          </Row>

          <Row>
            <Row className="center">
              <Col m={4} className="offset-m4">
                <ProfileImage src={props.imagePreview} className="responsive-img circle"/>
              </Col>
            </Row>
            <Col m={12} s={12} offset="">
              <div className="form">
                <div>
                  <LocalForm onSubmit={(values) => props.updateProfile(values) }
                    initialState={{
                      name: props.name,
                      email: props.email,
                      username: props.username,
                      id: props.id,
                      imagePreview: props.imagePreview
                    }}
                  >
                    <Control.file model=".photo"
                        onChange={(value) => props.encodeFile(value)}
                        style={{ 'marginBottom': '20px'}}
                    />
                    <Control.text model=".name"
                      name="name"
                      placeholder="Nome"
                    />
                    <Control.text model=".email"
                      name="email"
                      placeholder="E-mail"
                    />
                    <Control.text model=".username"
                      name="username"
                      placeholder="Username..."
                    />
                    <div className="right">
                      <button type="submit" className="blue btn grey darken-2">Atualizar</button>
                    </div>
                  </LocalForm>
                </div>
              </div>
            </Col>
          </Row>

          <Row>
            <Col m={8} s={8} offset="">
              <b className="grey-text text-darken-1">Troca de Senha</b>
            </Col>
          </Row>

          <Row>
            <Col m={12} s={12} offset="">
              <div className="form">
                <div>
                  <LocalForm onSubmit={(values) => props.updatePassword(values) } initialState={{ id: props.id, }} >
                    <Control.text model=".password"
                      name="password"
                      placeholder="Senha"
                    />
                    <Control.text model=".password_confirmation"
                      name="password_confirmation"
                      placeholder="Confirmação de senha"
                    />
                    <div className="right">
                      <button type="submit" className="blue btn grey darken-2">Atualizar</button>
                    </div>
                  </LocalForm>
                </div>
              </div>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  </div>
);

export default ProfileEdit;
