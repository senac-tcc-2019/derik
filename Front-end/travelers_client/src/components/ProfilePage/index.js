import React from "react";
import { Card, Row, Col, Container } from 'react-materialize';
import UserInfoContainer from '../../containers/UserInfoContainer';
import HeaderContainer from '../../containers/HeaderContainer';
import PostListContainer from '../../containers/PostListContainer';

const ProfilePage = () => (
  <div>
    <HeaderContainer/>
    <Container>
      <Row>
        <Col s={12} m={3}>
          <UserInfoContainer/>
        </Col>
        <Col s={12} m={9}>
          <Card>
            <PostListContainer/>
          </Card>
        </Col>
      </Row>
    </Container>
  </div>
);

export default ProfilePage;
