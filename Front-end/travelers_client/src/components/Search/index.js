import React from "react";
import { Row, Col, Container } from 'react-materialize';
import HeaderContainer from '../../containers/HeaderContainer';
import UserInfo from '../../components/UserInfo'
import SearchContainer from '../../containers/SearchContainer';

const SearchMessage = (props) => {
  if(props.search_users || props.msg) {
    return (
      <p style={{ textAlign: 'center', textDecoration: 'underline gray', color: 'gray' }}><i> Não foram encontrados usuários em sua pesquisa! </i></p>
    )
  }
  else {
    return(
      <p style={{ textAlign: 'center', textDecoration: 'underline gray', color: 'gray' }}><i> Foram encontrados {  Object.entries(props).length } usuários em sua pesquisa! </i></p>
    )
  }
}

const Search = (props) => (
  <div>
    <HeaderContainer />
      <Container>
        <Row>
          <SearchContainer showSearchBar={true} />
        </Row>
        <Row>
          <SearchMessage {...props}/>
        </Row>
        <Row>
          <Col s={12} m={12}>
            {
              !props.msg && !props.search_users &&
              Object.entries(props).map((user) =>
                <Col s={3} m={3} key={user[0]}>
                  <UserInfo {...user[1]} />
                </Col>
              )
            }
          </Col>
        </Row>
      </Container>
  </div>
);

export default Search;
