import React from "react";
import { Container, Icon } from 'react-materialize';
import styled from 'styled-components';

const InputStyled = styled.input`
  padding-left: 15px !important;
  border: 1px solid #c1b8b8 !important;
  border-radius: 3px !important;
  width: 85% !important;
`;

const SearchBar = (props) => (
  <Container>
    <div className="input-field">
      <Icon className="material-icons prefix">search</Icon>
      <InputStyled type="search" name="search" placeholder="Pesquise por usuário" onKeyDown={(event) => props.postSearch(event)} />
    </div>
  </Container>
)

export default SearchBar;
