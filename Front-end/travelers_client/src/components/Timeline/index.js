import React from "react";
import { Row, Col, Container } from 'react-materialize';
import HeaderContainer from '../../containers/HeaderContainer';
import UserInfoContainer from '../../containers/UserInfoContainer';
import PostListContainer from '../../containers/PostListContainer';
import SearchContainer from '../../containers/SearchContainer';
import PostFilterContainer from '../../containers/PostFilterContainer';

const Timeline = () => (
  <div>
    <HeaderContainer />
      <Container>
        <Row>
          <SearchContainer showSearchBar={true} />
        </Row>
        <Row>
          <Col s={12} m={3}>
            <UserInfoContainer hideFollow={true}/>
          </Col>
          <Col s={12} m={6}>
            <PostListContainer/>
          </Col>
          <Col s={12} m={3}>
            <PostFilterContainer/>
          </Col>
        </Row>
      </Container>
  </div>
);

export default Timeline;
