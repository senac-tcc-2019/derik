import React from "react";
import { Row, Col, Card, Icon} from 'react-materialize';
import styled from 'styled-components';
import fake_avatar from '../../images/avatar.png';
import UserFollowContainer from '../../containers/UserFollowContainer';
import UserUnFollowContainer from '../../containers/UserUnFollowContainer';

const Avatar = styled.img`
  margin-top: 8px;
  width: 80%`
;

const UserInfo = (props) => (
  <Card>
    <Row>
      <Col m={8} s={8} offset="m2 s2" className="center">
        <a href={`/user/${props.id}`}>
          <Avatar src={(props.photo && props.photo.url) ? props.photo.profile.url : fake_avatar } className="responsive-img circle m10" />
        </a>
      </Col>
    </Row>
    <Row>
      <Col m={9} s={9}>
        <b className="grey-text text-darken-2">{ props.name }</b>
      </Col>
      {
        !props.hideFollow &&
        <Col m={3} s={3}>
          { props.followed ? <UserUnFollowContainer userUnfollow={props} /> : <UserFollowContainer userFollow={props}/> }
        </Col>
      }
    </Row>
    <Row>
      <Col m={12}>
        { props.username }
      </Col>
    </Row>
    <Row>
      <Col m={4}>
        <Row className="tooltipped" data-tooltip="Posts">
          <Col m={5}>
            <Icon>message</Icon>
          </Col>
          <Col m={6}>
            { props.posts_count }
          </Col>
        </Row>
      </Col>
      <Col m={4}>
        <Row className="tooltipped" data-tooltip="Followers">
          <Col m={5}>
            <Icon>people</Icon>
          </Col>
          <Col m={6}>
            { props.followers_count }
          </Col>
        </Row>
      </Col>
      <Col m={4}>
        <Row className="tooltipped" data-tooltip="Followed">
          <Col m={5}>
            <Icon>people_outline</Icon>
          </Col>
          <Col m={6}>
            { props.following_count }
          </Col>
        </Row>
      </Col>
    </Row>
  </Card>
);

export default UserInfo;
