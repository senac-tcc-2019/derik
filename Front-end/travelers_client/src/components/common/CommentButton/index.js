import styled from 'styled-components';

const CommentButton = styled.button`
  background-color: #D3D3D3 !important;
  color: gray;
  -webkit-appearance: media-slider;
  border-radius: 3px;
  font-size: small;
  cursor: pointer;
`;

export default CommentButton;
