import { createStore, applyMiddleware } from 'redux';
import Reducers from './reducers';
import thunk from 'redux-thunk';
// Routes imports
import { connectRouter, routerMiddleware } from 'connected-react-router';
import history from './routes/history';

const middlewares = [
  thunk,
  routerMiddleware(history)
];

const Store = createStore(connectRouter(history)(Reducers), applyMiddleware(...middlewares));

export default Store;
