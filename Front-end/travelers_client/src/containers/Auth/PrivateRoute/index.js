import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCurrentUser } from '../actions';

class PrivateRoute extends Component {
  componentDidMount(){
    if(localStorage.jwt){
      this.props.getCurrentUser();
    }
  }

  render() {
    return (
      <Route
        render={props =>
          localStorage.jwt ? (
            this.props.current_user.id ? (
              <this.props.component {...this.props} />
            ) : 'Carregando...'
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    )
  }
}

function mapStateToProps(state) {
  return {
    current_user: state.current_user
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getCurrentUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute)
