import API from '../../../api.js';
import { logIn } from '../SignInContainer/actions.js';

export function register({name, last_name, username, email, password, country_id, state_id, city_id}) {
  const params = {"user": {name, last_name, username, email, password, country_id, state_id, city_id}}
  const request = API.post('/users', params);

  return (dispatch) => {
    request.then(
      resp => {
        dispatch(logIn({email, password}))
      },
      error => {
        window.M.toast({ html: 'Desculpe mas não foi possível registrar o usuário. Tente novamente!' })
      }
    );
  };
}
