import React from 'react'
import SignUp from '../../../components/Auth/SignUp'
import { register } from './actions';
import { getCountries, getStatesFromCountry, getCitiesFromState } from '../.././RegionsContainer/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class SignUpContainer extends React.Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getStates = this.getStates.bind(this)
    this.getCities = this.getCities.bind(this)
  }

  componentDidMount(){
    this.props.getCountries()
  }

  handleSubmit(form) {
    this.props.register(form)
  }

  getStates(event) {
    this.props.getCitiesFromState(0)
    this.props.getStatesFromCountry(event.target.value)
  }

  getCities(event) {
    this.props.getCitiesFromState(event.target.value)
  }

  render() {
    return <SignUp handleSubmit={this.handleSubmit}
                  countries={this.props.countries}
                  states={this.props.states}
                  getStates={this.getStates}
                  cities={this.props.cities}
                  getCities={this.getCities}
            />
  }
}

function mapStateToProps(state) {
  return {
    countries: state.countries,
    states: state.states,
    cities: state.cities
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ register, getCountries, getStatesFromCountry, getCitiesFromState }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpContainer)
