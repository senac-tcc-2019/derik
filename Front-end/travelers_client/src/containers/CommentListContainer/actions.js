import API from '../../api.js';
import { DELETE_COMMENT, ADD_COMMENT, UPDATE_COMMENT } from './constants';

export function createComment(props) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.post(`/comments`, {body: props.body, post_id: props.post}, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: ADD_COMMENT, payload: resp.data }, window.M.toast({ html: 'Comentário criado com suceddo!' })),
      error => window.M.toast({ html: 'Não foi possível criar o comentário.' })
    )
  };
}

export function updateComment(props) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.put(`/comments/${props.id}`, {body: props.body}, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: UPDATE_COMMENT, payload: resp.data }, window.M.toast({ html: 'O comentário foi atualizado com sucesso!' })),
      error => window.M.toast({ html: 'Não foi possível atualizar o comentário.' })
    )
  };
}

export function deleteComment(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.delete(`/comments/${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: DELETE_COMMENT, payload: id }, window.M.toast({ html: 'Comentário excluído com sucesso!' })),
      error => window.M.toast({ html: 'Não foi possível excluir o comentário.' })
    )
  };
}
