export const DELETE_COMMENT = 'comment_list/DELETE_COMMENT';
export const ADD_COMMENT = 'comment_list/ADD_COMMENT';
export const UPDATE_COMMENT = 'comment_list/UPDATE_COMMENT';
export const FETCH_COMMENTS = 'comment_list/FETCH_COMMENTS';
