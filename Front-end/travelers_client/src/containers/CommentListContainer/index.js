import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createComment, deleteComment, updateComment } from './actions.js';
import CommentUnit from '../../components/CommentUnit';
import CommentNew from '../../components/CommentNew';

class CommentListContainer extends Component {
  constructor(){
    super()
    this.postComment = this.postComment.bind(this)
    this.deleteComment = this.deleteComment.bind(this)
    this.putComment = this.putComment.bind(this)
  }

  postComment(values){
    let value = { body: values.event.target.value, post: values.postId };
    if(values.event.keyCode === 13) { // is enter key?
      this.props.createComment(value)
      values.setCommentsCount(this.props.post_comments.length + 1)
      values.event.target.value = ""
    }
  }

  putComment(values) {
    let value = { body: values.event.target.value, id: values.commentId };
    if(values.event.keyCode === 13) { // is enter key?
      this.props.updateComment(value)
      values.setShowComments(false)
    }
  }

  deleteComment(id, setCommentsCount){
    this.props.deleteComment(id)
    setCommentsCount(this.props.post_comments.length - 1)
  }

  render() {
    let comment_list = this.props.post_comments;
    return (
      <Fragment>
        {
          comment_list.length > 0 && !this.props.createComments &&
          comment_list.map((comment, key) => {
            if (comment.post_id === this.props.postId) {
              return(
                <CommentUnit {...comment}
                             key={key}
                             deleteComment={this.deleteComment}
                             putComment={this.putComment}
                             setShowComments={this.props.setShowComments}
                             current_user={this.props.current_user}
                             setCommentsCount={this.props.setCommentsCount}
                />
              )
            }
            else { return null }
          })
        }

        {
          this.props.createComments &&
          <CommentNew postComment={this.postComment} postId={this.props.postId} setCommentsCount={this.props.setCommentsCount}/>
        }
      </Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts,
    current_user: state.current_user,
    post_comments: state.post_comments
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createComment, deleteComment, updateComment }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(CommentListContainer)
