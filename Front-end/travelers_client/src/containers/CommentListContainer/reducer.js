import { ADD_COMMENT, UPDATE_COMMENT, DELETE_COMMENT, FETCH_COMMENTS } from '.././CommentListContainer/constants';

const initialState = { post_comments: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_COMMENTS:
      return action.payload
    case DELETE_COMMENT:
      return state.filter(comment => comment.id !== action.payload)
    case ADD_COMMENT:
      return [action.payload, ...state]
    case UPDATE_COMMENT:
      state.forEach((comment, index) => {
        if(comment.id === action.payload.id) {
          comment.body = action.payload.body
        }
      })
      return action.payload
    default:
      return state;
  }
}
