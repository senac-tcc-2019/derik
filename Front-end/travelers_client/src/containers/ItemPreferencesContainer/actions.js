import API from '../../api.js';
import { GET_ITEM_PREFERENCES, POST_ITEM_PREFERENCE, DELETE_ITEM_PREFERENCE } from './constants';

export function postItemPreference(item_id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.post('/item_preferences', {item_preference: {...item_id}}, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: POST_ITEM_PREFERENCE, payload: resp.data }, window.M.toast({ html: 'Preferência adicionada com sucesso!' })),
      error => window.M.toast({ html: 'Desculpe, não foi possível criar a preferência. Tente novamente!' })
    )
  };
}

export function getItemPreference(item_id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.get('/item_preferences', {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: GET_ITEM_PREFERENCES, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de preferência, por favor, atualize a página!' })
    )
  };
}

export function deleteItemPreference(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.delete(`/item_preferences/${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: DELETE_ITEM_PREFERENCE, payload: id}, window.M.toast({ html: 'Preferência excluída com sucesso!' })),
      error => window.M.toast({ html: 'Desculpe, não foi possível excluir a preferência. Tente novamente!' })
    )
  };
}
