export const POST_ITEM_PREFERENCE = 'item_preference/POST_ITEM_PREFERENCE';
export const GET_ITEM_PREFERENCES = 'item_preference/GET_ITEM_PREFERENCES';
export const DELETE_ITEM_PREFERENCE = 'item_preference/DELETE_ITEM_PREFERENCE';
