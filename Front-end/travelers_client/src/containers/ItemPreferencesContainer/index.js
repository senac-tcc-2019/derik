import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCountries, getStatesFromCountry, getCitiesFromState } from '.././RegionsContainer/actions';
import { getTypes } from '.././TypeContainer/actions';
import { postItemPreference } from './actions';
import ItemPreferences from '../../components/ItemPreferences';

class ItemPreferencesContainer extends Component {

  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getStates = this.getStates.bind(this)
    this.getCities = this.getCities.bind(this)
  }

  componentDidMount(){
    this.props.getCountries()
    this.props.getTypes()
  }

  handleSubmit(form) {
    this.props.postItemPreference(form)
  }

  getStates(event) {
    this.props.getCitiesFromState(0)
    this.props.getStatesFromCountry(event.target.value)
  }

  getCities(event) {
    this.props.getCitiesFromState(event.target.value)
  }

  render() {
    return (
      <ItemPreferences handleSubmit={this.handleSubmit}
                       countries={this.props.countries}
                       states={this.props.states}
                       getStates={this.getStates}
                       cities={this.props.cities}
                       getCities={this.getCities}
                       types={this.props.types}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    countries: state.countries,
    states: state.states,
    cities: state.cities,
    types: state.types
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getCountries, getStatesFromCountry, getCitiesFromState, postItemPreference, getTypes }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemPreferencesContainer)
