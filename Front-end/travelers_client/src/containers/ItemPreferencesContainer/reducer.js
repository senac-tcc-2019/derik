import { GET_ITEM_PREFERENCES, POST_ITEM_PREFERENCE, DELETE_ITEM_PREFERENCE } from './constants';

const initialState = { item_preferences: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ITEM_PREFERENCES:
      return action.payload
    case POST_ITEM_PREFERENCE:
      return [action.payload, ...state]
    case DELETE_ITEM_PREFERENCE:
      return state.filter(item_preference => item_preference.id !== action.payload)
    default:
      return state;
  }
}
