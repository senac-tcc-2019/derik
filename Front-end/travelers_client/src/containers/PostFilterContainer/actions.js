import API from '../../api.js';
import { FETCH_POSTS } from '../PostListContainer/constants';

export function getPostsFiltered(type_id, country_id, state_id, city_id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.post(`/post_filter`, {type_id: type_id, country_id: country_id, state_id: state_id, city_id: city_id}, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: FETCH_POSTS, payload: resp.data }),
      error => window.M.toast({ html: 'Houve algum problema no carregamento das postagens. Por favor, tente novamente!' })
    )
  };
}
