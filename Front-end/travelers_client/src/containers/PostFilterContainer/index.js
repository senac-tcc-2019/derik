import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCountries, getStatesFromCountry, getCitiesFromState } from '.././RegionsContainer/actions';
import { getTypes } from '.././TypeContainer/actions';
import { getPostsFiltered } from './actions';
import PostFilter from '../../components/PostFilter'


class PostListContainer extends Component {
  constructor(){
    super()
    this.getStates = this.getStates.bind(this)
    this.getCities = this.getCities.bind(this)
    this.postFilter = this.postFilter.bind(this)
  }

  componentDidMount(){
    this.props.getCountries()
    this.props.getTypes()
  }

  getStates(event) {
    this.props.getCitiesFromState(0)
    this.props.getStatesFromCountry(event.target.value)
  }

  getCities(event) {
    this.props.getCitiesFromState(event.target.value)
  }

  postFilter(values) {
    const { type_id, country_id, state_id, city_id } = values
    this.props.getPostsFiltered(type_id, country_id, state_id, city_id)
  }

  render() {
    return (
      <div>
        <PostFilter countries={this.props.countries}
                    states={this.props.states}
                    getStates={this.getStates}
                    cities={this.props.cities}
                    getCities={this.getCities}
                    types={this.props.types}
                    postFilter={this.postFilter}
       />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    current_user: state.current_user,
    countries: state.countries,
    states: state.states,
    cities: state.cities,
    types: state.types
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getCountries, getStatesFromCountry, getCitiesFromState, getTypes, getPostsFiltered }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(PostListContainer)
