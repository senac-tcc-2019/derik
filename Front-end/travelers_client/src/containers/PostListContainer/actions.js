import API from '../../api.js';
import { DELETE_POST, ADD_POST, POST_IMAGE_PREVIEW } from './constants';
import { FETCH_COMMENTS } from '.././CommentListContainer/constants';

export function deletePost(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.delete(`/posts/${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: DELETE_POST, payload: id }, window.M.toast({ html: 'Postagem excluída com sucesso!' })),
      error => window.M.toast({ html: 'Desculpe, não foi possível exluir a postagem. Tente novamente!' })
    )
  };
}

export function createPost(values, photo) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const params = { "post": values }
  const request = API.post(`/posts`, params, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: ADD_POST, payload: resp.data, photo: photo }, window.M.toast({ html: 'Postagem criada com sucesso!' })),
      error => window.M.toast({ html: 'Desculpe, não foi possível criar a postagem. Tente novamente!' })
    )
  };
}

export function getCommentList(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.get(`/post_comments/${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: FETCH_COMMENTS, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de comentários. Por favor, atualize a página.' })
    )
  };
}

export function postImagePreview(imagePreview) {
  return {
    type: POST_IMAGE_PREVIEW,
    payload: imagePreview
  }
}
