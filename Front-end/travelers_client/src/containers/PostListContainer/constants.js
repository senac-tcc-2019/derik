export const FETCH_POSTS = 'post_list/FETCH_POSTS';
export const DELETE_POST = 'post_list/DELETE_POST';
export const ADD_POST = 'post_list/ADD_POST';
export const POST_IMAGE_PREVIEW = 'post_list/POST_IMAGE_PREVIEW';
