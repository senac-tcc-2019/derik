import React, { Component } from 'react';
import PostUnit from '../../components/PostUnit'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card } from 'react-materialize'
import { deletePost, createPost, getCommentList, postImagePreview } from './actions.js';
import { getCountries, getStatesFromCountry, getCitiesFromState } from '.././RegionsContainer/actions';
import { getTypes } from '.././TypeContainer/actions';
import PostNew from '../../components/PostNew';



class PostListContainer extends Component {
  constructor(){
    super()
    this.deletePost = this.deletePost.bind(this)
    this.postPost = this.postPost.bind(this)
    this.getCommentList = this.getCommentList.bind(this)
    this.getStates = this.getStates.bind(this)
    this.getCities = this.getCities.bind(this)
    this.encodeFile = this.encodeFile.bind(this)
    this.getBase64 = this.getBase64.bind(this)
  }

  componentDidMount(){
    this.props.getCountries()
    this.props.getTypes()
  }

  getStates(event) {
    this.props.getCitiesFromState(0)
    this.props.getStatesFromCountry(event.target.value)
  }

  getCities(event) {
    this.props.getCitiesFromState(event.target.value)
  }

  deletePost(id){
    this.props.deletePost(id)
  }

  postPost(values){
    var newValues = {...values}
    newValues.photo = this.props.postImage;
    this.props.createPost(newValues, newValues.photo)
  }

  getCommentList(id){
    this.props.getCommentList(id)
  }

  getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result)
    };
    reader.onerror = function (error) {
      window.M.toast({ html: 'Desculpe, não foi possível atualizar a foto. Tente novamente!' })
    };
  }

  encodeFile(e){
    this.getBase64(e.target.files[0], (result) => {
      this.props.postImagePreview(result)
    });
  }

  render() {
    var post_list = this.props.posts.length ? this.props.posts : []
    return (
      <div>
        {this.props.current_user.id === this.props.user.id &&
          <PostNew postPost={this.postPost}
                   countries={this.props.countries}
                   states={this.props.states}
                   getStates={this.getStates}
                   cities={this.props.cities}
                   getCities={this.getCities}
                   types={this.props.types}
                   encodeFile={this.encodeFile}
                   postImage={this.props.postImage}
          />
        }
        {post_list.map((post, i) =>
          <PostUnit {...post}
                    key={i}
                    deletePost={this.deletePost}
                    current_user={this.props.current_user}
                    getCommentList={this.getCommentList}
                    postComments={this.props.post_comments}
          />
        )}
        {
          post_list.length === 0 &&
          <Card style={{ textAlign: 'center', color: 'grey' }}>
            <p>
              Não foram encontradas postagens ou nenhuma postagem foi publicada. Aproveite, seja o primeiro a postar! =)
            </p>
          </Card>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts,
    current_user: state.current_user,
    user: state.user,
    post_comments: state.post_comments,
    postImage: state.postImage,
    countries: state.countries,
    states: state.states,
    cities: state.cities,
    types: state.types
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ deletePost, createPost, getCommentList, postImagePreview, getCountries, getStatesFromCountry, getCitiesFromState, getTypes }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(PostListContainer)
