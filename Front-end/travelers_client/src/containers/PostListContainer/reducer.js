import { FETCH_POSTS, DELETE_POST, ADD_POST } from './constants';

const initialState = { posts: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return action.payload
    case DELETE_POST:
      return state.filter(post => post.id !== action.payload)
    case ADD_POST:
      action.payload.photo.url = action.photo
      return [action.payload, ...state]
    default:
      return state;
  }
}
