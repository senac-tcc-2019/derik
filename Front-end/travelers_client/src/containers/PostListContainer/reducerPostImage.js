import { POST_IMAGE_PREVIEW } from './constants';

const initialState = { postImage: {} };

export default function(state = initialState, action) {
  switch (action.type) {
    case POST_IMAGE_PREVIEW:
      return action.payload
    default:
      return state;
  }
}
