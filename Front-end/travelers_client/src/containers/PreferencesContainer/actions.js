import API from '../../api.js';

export function updateDescription({id, description}) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const params = {"preference": {description}}
  const request = API.put(`/preferences/${id}`, params, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => {
        window.M.toast({ html: 'A descrição foi atualizada com sucesso!' });
      },
      error => {
        window.M.toast({ html: 'Desculpe, não foi possível atualizar o perfil. Tente novamente!' });
      }
    );
  };
}
