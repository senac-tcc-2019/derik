import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateDescription } from './actions';
import { getItemPreference, deleteItemPreference } from '../ItemPreferencesContainer/actions';
import { bindActionCreators } from 'redux';
import Preferences from '../../components/Preferences';

class PreferencesContainer extends Component {

  constructor() {
    super()
    this.updatePreference = this.updatePreference.bind(this)
    this.delItemPreference = this.delItemPreference.bind(this)
  }

  componentDidMount()  {
    this.props.getItemPreference()
  }

  delItemPreference(id) {
    this.props.deleteItemPreference(id)
  }

  updatePreference(values) {
    this.props.updateDescription(values)
  }

  render() {
    return (
      <Preferences {...this.props.current_user}
                  updatePreference={this.updatePreference}
                  itemPreferences={this.props.item_preferences}
                  delItemPreference={this.delItemPreference}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    current_user: state.current_user,
    item_preferences: state.item_preferences
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ updateDescription, getItemPreference, deleteItemPreference }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PreferencesContainer)
