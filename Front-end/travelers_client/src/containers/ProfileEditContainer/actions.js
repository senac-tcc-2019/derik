import API from '../../api.js';
import { UPDATE_IMAGE_PREVIEW } from './constants';

export function updateUserInfo({id, name, email, username, photo}) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const params = {"user": {name, email, username, photo}}
  const request = API.put(`/users/${id}`, params, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => {
        window.M.toast({ html: 'Perfil atualizado com sucesso!' });
      },
      error => {
        window.M.toast({ html: 'Desculpe, não foi possível atualizar o perfil. Tente novamente!' });
      }
    );
  };
}

export function updateUserPassword({id, password, password_confirmation}) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const params = {"user": {password, password_confirmation}}
  const request = API.put(`/users/${id}`, params, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => {
        window.M.toast({ html: 'Senha atualizada com sucesso!' })
      },
      error => {
        window.M.toast({ html: 'Desculpe, não foi possível atualizar a senha. Tente novamente!' })
      }
    );
  };
}

export function updateImagePreview(imagePreview) {
  return {
    type: UPDATE_IMAGE_PREVIEW,
    payload: imagePreview
  }
}
