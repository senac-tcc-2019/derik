import API from '../../api.js';
import { UPDATE_USER_INFO } from '../UserInfoContainer/constants';
import { FETCH_POSTS } from '../PostListContainer/constants';

export function getUserInfo(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.get(`/users/${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: UPDATE_USER_INFO, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar os dados do perfil do usuário. Por favor, atualize a página!' })
    )
  };
}

export function getPostList(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.get(`/posts?user_id=${id}`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: FETCH_POSTS, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de postagens. Por favor, atualize a página!' })
    )
  };
}
