import API from '../../api.js';
import { GET_COUNTRIES, GET_STATES, GET_CITIES } from '../RegionsContainer/constants';

export function getCountries() {
  const request = API.get(`/regions/countries`);

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: GET_COUNTRIES, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de regioões. Por favor, atualize a página!' })
    )
  };
}

export function getStatesFromCountry(id) {
  const request = API.post(`/regions/states?id=${id}`);

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: GET_STATES, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de regioões. Por favor, atualize a página!' })
    )
  };
}

export function getCitiesFromState(id) {
  const request = API.post(`/regions/cities?id=${id}`);

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: GET_CITIES, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de regioões. Por favor, atualize a página!' })
    )
  };
}
