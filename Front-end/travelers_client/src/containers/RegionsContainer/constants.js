export const GET_COUNTRIES = 'regions/GET_COUNTRIES';
export const GET_STATES = 'regions/GET_STATES';
export const GET_CITIES = 'regions/GET_CITIES';
