import { GET_CITIES } from './constants';

const initialState = { cities: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_CITIES:
      return action.payload
    default:
      return state;
  }
}
