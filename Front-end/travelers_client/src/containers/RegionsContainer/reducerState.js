import { GET_STATES } from './constants';

const initialState = { states: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_STATES:
      return action.payload
    default:
      return state;
  }
}
