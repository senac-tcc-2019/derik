import API from '../../api.js';
import { SEARCH_USERS } from './constants';
import { push } from 'connected-react-router';

export function searchUsers(user_name) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const params = {"user_name": user_name}
  const request = API.post(`/search`, params, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: SEARCH_USERS, payload: resp.data }), dispatch(push('/search')),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de usuários. Por favot, atualize a página e tente novamente!' })
    )
  };
}
