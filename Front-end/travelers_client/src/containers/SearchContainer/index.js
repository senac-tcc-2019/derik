import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUserInfo } from '../TimelineContainer/actions';
import { searchUsers } from './actions';
import Search from '../../components/Search';
import SearchBar from '../../components/SearchBar';

class SearchContainer extends Component {
  constructor(){
    super()
    this.postSearch = this.postSearch.bind(this)
  }

  componentDidMount(){
    this.props.setUserInfo(this.props.current_user)
  }

  postSearch(event){
    if(event.keyCode === 13) { // is enter key?
      this.props.searchUsers(event.target.value)
      event.target.value = ""
    }
  }

  render(){
    return(
      <Fragment>
        {
          this.props.showSearchBar &&
          <SearchBar postSearch={this.postSearch} />
        }
        {
          !this.props.showSearchBar &&
          <Search {...this.props.search_users} />
        }
      </Fragment>
    )
  }

}

function mapStateToProps(state) {
  return {
    current_user: state.current_user,
    search_users: state.search_users
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ setUserInfo, searchUsers }, dispatch)
}

export default connect(mapStateToProps , mapDispatchToProps)(SearchContainer)
