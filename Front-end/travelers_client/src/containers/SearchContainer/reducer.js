import { SEARCH_USERS } from './constants';

const initialState = { search_users: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USERS:
      return action.payload
    default:
      return state;
  }
}
