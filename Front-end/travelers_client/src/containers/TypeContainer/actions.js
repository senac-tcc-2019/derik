import API from '../../api.js';
import { GET_TYPES} from './constants';

export function getTypes() {
  const request = API.get(`/types`);

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: GET_TYPES, payload: resp.data }),
      error => window.M.toast({ html: 'Desculpe, não foi possível carregar a lista de tipos de local. Por favor, atualize a página!' })
    )
  };
}
