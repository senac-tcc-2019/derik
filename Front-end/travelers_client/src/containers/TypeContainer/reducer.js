import { GET_TYPES } from './constants';

const initialState = { types: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TYPES:
      return action.payload
    default:
      return state;
  }
}
