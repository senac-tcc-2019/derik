import API from '../../api.js';
import { FOLLOW_USER } from './constants';

export function followUser(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.post(`/users/${id}/follow`, {id: id}, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: FOLLOW_USER, payload: resp.data },
        window.M.toast({ html: 'Usuário adicionado a lista de favoritos com sucesso!' }),
        setTimeout(() => document.location.reload(), 800)
      ),
      error => window.M.toast({ html: 'Desculpe, não foi possível adicionar o usuário à sua lista de favoritos. Tente novamente!' })
    )
  };
}
