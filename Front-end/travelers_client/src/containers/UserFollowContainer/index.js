import React, { Component } from 'react';
import { UserFollow } from '../../components/Follows';
import { followUser } from './actions.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class UserFollowContainer extends Component {
  constructor(){
    super()
    this.follow = this.follow.bind(this)
  }

  follow (values){
    let id = values.id;
    this.props.followUser(id)
 }

  render() {
    return (
      <UserFollow follow={this.follow} user={this.props.userFollow} />
   );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ followUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserFollowContainer)
