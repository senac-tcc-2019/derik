import API from '../../api.js';
import { UNFOLLOW_USER } from './constants';

export function unFollowUser(id) {
  const headers =  { 'Authorization': 'Bearer ' + localStorage.getItem('jwt') };
  const request = API.delete(`/users/${id}/follow`, {headers: headers});

  return (dispatch) => {
    request.then(
      resp => dispatch({ type: UNFOLLOW_USER, payload: resp.data },
        window.M.toast({ html: 'Você removeu este usuário da sua lista de favoritos!' }),
        setTimeout(() => document.location.reload(), 800)
      ),
      error => window.M.toast({ html: 'Desculpe, não foi possível remover o usuário da lista de favoritos. Tente novamente!' })
    )
  };
}
