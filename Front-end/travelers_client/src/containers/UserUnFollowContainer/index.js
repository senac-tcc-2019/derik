import React, { Component } from 'react';
import { UserUnFollow } from '../../components/Follows';
import { unFollowUser } from './actions.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class UserUnFollowContainer extends Component {
  constructor(){
    super()
    this.unfollow = this.unfollow.bind(this)
  }

  unfollow (values) {
    let id = values.id;
    this.props.unFollowUser(id);
  }

  render() {
    return (
      <UserUnFollow unfollow={this.unfollow} user={this.props.userUnfollow} />
   );
  }
}
function mapStateToProps(state, ownProps) {
  return {
    user: state.user
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ unFollowUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserUnFollowContainer)
