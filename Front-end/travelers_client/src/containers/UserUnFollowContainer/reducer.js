import { UNFOLLOW_USER } from './constants';

const initialState = {{}};

export default function(state = initialState, action) {
  switch (action.type) {
    case UNFOLLOW_USER:
      return action.payload
    default:
      return state;
  }
}
