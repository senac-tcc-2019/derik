import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from './routes/history';
import CurrentUserReducer from './containers/Auth/reducer.js';
import UserInfoContainer from './containers/UserInfoContainer/reducer.js';
import ImagePreviewReducer from './containers/ProfileEditContainer/reducer.js';
import PostListReducer from './containers/PostListContainer/reducer.js';
import PostImagePreviewReducer from './containers/PostListContainer/reducerPostImage.js';
import CommentListReducer from './containers/CommentListContainer/reducer.js';
import SearchReducer from './containers/SearchContainer/reducer.js';
import CountryReducer from './containers/RegionsContainer/reducerCountry.js';
import StateReducer from './containers/RegionsContainer/reducerState.js';
import CityReducer from './containers/RegionsContainer/reducerCity.js';
import ItemPreferencesReducer from './containers/ItemPreferencesContainer/reducer.js';
import TypeReducer from './containers/TypeContainer/reducer.js';


const rootReducer = combineReducers({
  current_user: CurrentUserReducer,
  user: UserInfoContainer,
  item_preferences: ItemPreferencesReducer,
  imagePreview: ImagePreviewReducer,
  posts: PostListReducer,
  postImage: PostImagePreviewReducer,
  post_comments: CommentListReducer,
  search_users: SearchReducer,
  countries: CountryReducer,
  states: StateReducer,
  cities: CityReducer,
  types: TypeReducer,
  router: connectRouter(history)
});

export default rootReducer;
