import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import history from './history';
import HomePageContainer from '../containers/HomePageContainer';
import ProfileEditContainer from '../containers/ProfileEditContainer';
import ProfilePageContainer from '../containers/ProfilePageContainer';
import TimelineContainer from '../containers/TimelineContainer';
import SearchContainer from '../containers/SearchContainer';
import PreferencesContainer from '../containers/PreferencesContainer';
import PrivateRoute from '../containers/Auth/PrivateRoute';

const Routes = () => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact path="/" component={HomePageContainer} />
      <Route exact path="/user/:id" component={ProfilePageContainer} />
      <PrivateRoute exact path="/search" component={SearchContainer} />
      <PrivateRoute exact path="/timeline" component={TimelineContainer} />
      <PrivateRoute exact path="/user/:id/edit" component={ProfileEditContainer} />
      <PrivateRoute exact path="/preferences" component={PreferencesContainer} />
    </Switch>
  </ConnectedRouter>
);

export default Routes;
