# Travelers
 O projeto Travelers trata-se de uma rede social voltada para pessoas que gostam de viajar e/ou conhecer novos lugares, e tem como objetivo trazer,
através da intereação de usuários, informações mais qualificadas sobre diferentes lugares para se viajar.

# Das Tecnologias utilizadas no projeto
  O presente projeto utiliza em sua API Ruby On Rails. Ao final deste readme estará disponível o link do projeto, já em funcionamento parcial para fins de teste, contudo, caso tenha-se
vontade de subir o projeto em sua própria máquina, após seguir as instalações descritas abaixo, será necessário configurar o front-end do projeto(instruções [Aqui](https://gitlab.com/senac-tcc-2019/derik/blob/master/Front-end/travelers_client/README.md)):
   - Ruby
   - Rails 
   - Mysql

 Para uma instalação simples e rápida dos programas necessários para rodar a API, indica-se seguir o passo a passo do site [GoRails](https://gorails.com/setup/ubuntu/19.04).
 
 Para executar a api basta seguir os passos abaixo: 
 No console instale o bundle:
  - gem install bundler

  Execute o bundle:
  - bundle install

  Crie o Banco de dados e insira as tabelas executando os seguintes comandos no console:
  - rails db:create
  - rails db:migrate

  Suba o servidor do Rails executando o seguinte comando no console:
  - rails s

# Documentação
 A API da aplicação se encontra disponível no Heroku, e para que seja possível testar suas requisões e retornos, basta utilizar a documentação disponibilizada abaixo. 
 - https://documenter.getpostman.com/view/4300157/S1a7UjaX

# Link do projeto em funcionamento parcial
 O projeto já está disponível para fins testes no link abaixo:
 - https://socialtravelers.herokuapp.com/

**Obs.:**
Ao criar um novo usuário, será necessário preencher o País, Estado e Cidade, porém, os mesmos ainda não estão totalmente prontos, por tanto, disponibilizamos um `id` específico para teste, basta
preencher os campos citados com o número `2`.
