module Api
  module V1
    class CommentsController < Api::V1::ApiController
      before_action { current_user }
      before_action :set_comment, except: :create
      before_action :authenticate_user, except: :show
      load_and_authorize_resource except: :show

      def index; end

      def create
        @comment = Comment.new(comment_params.merge(user: current_user))

        if @comment.save
          render json: @comment, status: :created
        else
          render json: { errors: @comment.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def show
        render json: @comment
      end

      def destroy
        @comment.destroy
      end

      def update
        if @comment.update(comment_params)
          render json: @comment
        else
          render json: { errors: @comment.errors.full_messages }, status: :unprocessable_entity
        end
      end

      private

        def set_comment
          @comment = Comment.find(params[:id])
        end

        def comment_params
          params.require(:comment).permit(:body, :post_id)
        end
    end
  end
end
