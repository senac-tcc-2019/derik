module Api
  module V1
    class ItemPreferencesController < Api::V1::ApiController
      before_action :authenticate_user
      before_action :set_item_preference, except: [:create, :index]

      def index
        @item_preferences = current_user.preference.item_preferences
        render json: @item_preferences
      end

      def create
        @item_preference = ItemPreference.new(item_preference_params.merge(user: current_user, preference: current_user.preference))

        if @item_preference.save
          render json: @item_preference, status: :created
        else
          render json: { errors: @item_preference.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def update
        if @item_preference.update(item_preference_params)
          render json: @item_preference
        else
          render json: { errors: @item_preference.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def show
        render json: @item_preference
      end

      def destroy
        @item_preference.destroy
      end

      private

      def set_item_preference
        @item_preference = current_user.preference.item_preferences.find(params[:id])
      end

      def item_preference_params
        params
          .require(:item_preference)
          .permit(:type_id, :city_id, :state_id, :country_id)
      end
    end
  end
end
