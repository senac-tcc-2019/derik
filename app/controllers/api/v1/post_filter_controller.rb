module Api
  module V1
    class PostFilterController < Api::V1::ApiController
      before_action :authenticate_user

      def index
        new_params = {}
        new_params[:country_id_eq] = params[:country_id] if params[:country_id]
        new_params[:state_id_eq] = params[:state_id] if params[:state_id]
        new_params[:city_id_eq] = params[:city_id] if params[:city_id]
        new_params[:type_id_eq] = params[:type_id] if params[:type_id]

        posts = Post.ransack(new_params).result.order(created_at: :desc)
        render json: posts.paginate(page: params[:page] || 1)
      end
    end
  end
end
