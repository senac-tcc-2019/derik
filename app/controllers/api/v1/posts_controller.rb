module Api
  module V1
    class PostsController < Api::V1::ApiController
      before_action { current_user }
      before_action :set_post, except: %i[create index]
      before_action :authenticate_user, except: [:show, :index, :post_comments]
      load_and_authorize_resource except: %i[index show post_comments]

      def index
        user = User.find params[:user_id]
        @posts = user.posts.paginate(page: params[:page] || 1)
        render json: @posts
      end

      def create
        @post = Post.new(post_params.merge(user: current_user))

        if @post.save
          render json: @post, status: :created
        else
          render json: { errors: @post.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def show
        render json: @post
      end

      def destroy
        @post.destroy
      end

      def update
        if @post.update(post_params)
          render json: @post
        else
          render json: { errors: @post.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def post_comments
        comments = @post.comments
        render layout: false, json: comments
      end

      private

        def set_post
          @post = Post.find(params[:id])
        end

        def post_params
          params.require(:post).permit(:body, :photo, :type_id, :city_id, :state_id, :country_id)
        end
    end
  end
end
