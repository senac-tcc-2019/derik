module Api
  module V1
    class PreferencesController < Api::V1::ApiController
      before_action { current_user }
      before_action :authenticate_user, :set_preference
      load_and_authorize_resource except: :show

      def update
        if @preference.update(user_params)
          render json: @preference
        else
          render json: { errors: @preference.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def show
        render json: @preference
      end

      private

      def set_preference
        @preference = current_user.preference
      end

      def user_params
        params
          .require(:preference)
          .permit(:description)
      end
    end
  end
end
