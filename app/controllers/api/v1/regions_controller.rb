module Api
  module V1
    class RegionsController < Api::V1::ApiController
      def countries
        countries = Country.all
        render json: countries
      end

      def states
        states = State.where(country_id: params[:id])
        render json: states
      end

      def cities
        cities = City.where(state_id: params[:id])
        render json: cities
      end
    end
  end
end
