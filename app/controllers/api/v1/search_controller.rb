module Api
  module V1
    class SearchController < Api::V1::ApiController
      before_action :authenticate_user

      def index
        if params[:user_name]
          users = User.all.where("name like ?", "%#{params[:user_name]}%").reject{|user| user == current_user}
          return render json: users.paginate(page: (params[:page] || 1)) if users.any?
          render json: { msg: 'Dont exist user with this name. Try again with other name!' }
        else
          render json: { msg: 'Please, insert a name of user to continue your search!' }
        end
      end
    end
  end
end
