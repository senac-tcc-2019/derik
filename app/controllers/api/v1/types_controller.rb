module Api
  module V1
    class TypesController < Api::V1::ApiController
      def index
        types = Type.all
        render json: types
      end
    end
  end
end
