class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, User
    can :read, Post
    can :read, Comment
    return unless user
    can :manage, User, id: user.id
    can :manage, Post, user_id: user.id
    can :manage, Comment, user_id: user.id
    can :manage, Preference, user_id: user.id
  end
end
