class City < ApplicationRecord
  belongs_to :state
  has_one :country, through: :state
  has_many :posts

  validates :name, presence: true
end
