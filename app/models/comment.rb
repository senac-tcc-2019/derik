class Comment < ApplicationRecord
  acts_as_votable
  belongs_to :user
  belongs_to :post

  validates :body, presence: true
end
