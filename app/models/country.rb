class Country < ApplicationRecord
  has_many :states, dependent: :destroy
  has_many :posts

  validates :name, presence: true
end
