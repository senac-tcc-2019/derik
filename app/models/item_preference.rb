class ItemPreference < ApplicationRecord
  belongs_to :preference
  belongs_to :type, optional: true
  belongs_to :city, optional: true
  belongs_to :state, optional: true
  belongs_to :country, optional: true

  has_one :user, through: :preference
end
