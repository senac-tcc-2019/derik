class Post < ApplicationRecord
  mount_base64_uploader :photo, PhotoUploader
  acts_as_votable
  
  belongs_to :user
  belongs_to :country
  belongs_to :state
  belongs_to :city
  belongs_to :type

  has_many :comments, dependent: :destroy

  validates :body, presence: true
  validates :photo, presence: true
end
