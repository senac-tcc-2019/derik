class Preference < ApplicationRecord
  belongs_to :user

  has_many :item_preferences
  accepts_nested_attributes_for :item_preferences, allow_destroy: true
end
