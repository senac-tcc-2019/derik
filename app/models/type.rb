class Type < ApplicationRecord
  has_many :item_preferences
  has_many :posts

  validates_uniqueness_of :name, presence: true
end
