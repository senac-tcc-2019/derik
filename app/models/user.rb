class User < ApplicationRecord
  mount_base64_uploader :photo, PhotoUploader
  has_secure_password

  belongs_to :city
  belongs_to :country
  belongs_to :state

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_one :preference, dependent: :destroy

  acts_as_followable
  acts_as_follower
  acts_as_voter

  validates :email, presence: true
  validates :last_name, presence: true
  validates :name, presence: true
  validates :username, presence: true

  after_create :create_preferences

  def timeline
    return posts_by_preferences if preference.item_preferences.any?
    timeline = Post.order(created_at: :desc)
  end

  def posts_by_preferences
    filter = prepare_filter
    Post.ransack(filter).result.order(created_at: :desc)
  end

  def prepare_filter
    filter = {
      type_id_in: [],
      country_id_in: [],
      state_id_in: [],
      city_id_in: []
    }

    preference.item_preferences.each do |item_preference|
      filter[:type_id_in] << item_preference.type_id if item_preference.type_id
      filter[:country_id_in] << item_preference.country_id if item_preference.country_id
      filter[:state_id_in] << item_preference.state_id if item_preference.state_id
      filter[:city_id_in] << item_preference.city_id if item_preference.city_id
    end

    filter
  end

  private

  def create_preferences
    Preference.create!(user_id: id)
  end
end
