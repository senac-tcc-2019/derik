
module Api
  module V1
    class ItemPreferenceSerializer < ActiveModel::Serializer
      attributes :id
      belongs_to :type
      belongs_to :country
      belongs_to :state
      belongs_to :city
    end
  end
end
