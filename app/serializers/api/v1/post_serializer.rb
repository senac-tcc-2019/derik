module Api
  module V1
    class PostSerializer < ActiveModel::Serializer
      attributes :id, :body, :photo, :likes_count, :comments_count, :comments, :post_date
      # TODO:  :liked
      belongs_to :user
      belongs_to :country
      belongs_to :state
      belongs_to :city
      belongs_to :type

      def post_date
        object.created_at.strftime('%d/%m/%y')
      end

      def user
        options = { each_serializer: Api::V1::UserSerializer }
        ActiveModelSerializers::SerializableResource.new(object.user, options)
      end

      def likes_count
        object.votes_for.size
      end

      def comments_count
        object.comments.count
      end

      # TODO:  def liked
      #   (current_user)? (current_user.liked? object) : false
      # end
    end
  end
end
