module Api
  module V1
    class StateSerializer < ActiveModel::Serializer
      attributes :id, :name
    end
  end
end
