module Api
  module V1
    class UserSerializer < ActiveModel::Serializer
      attributes :id, :name, :last_name, :username, :email, :country, :state, :city, :posts_count, :followers_count, :following_count, :photo, :followed
      has_one :preference

      def posts_count
        object.posts.count
      end

      def followers_count
        object.followers_by_type('User').count
      end

      def following_count
        object.following_users.count
      end

      def followed
        respond_to?(:current_user) ? current_user&.following?(object) : false
      end
    end
  end
end
