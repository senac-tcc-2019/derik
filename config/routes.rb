Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'user_token', to: 'user_token#create'
      post 'search', to: 'search#index'
      post 'post_filter', to: 'post_filter#index'
      post 'regions/states', to: 'regions#states'
      post 'regions/cities', to: 'regions#cities'

      get 'regions/countries', to: 'regions#countries'
      get 'timeline', to: 'timeline#index'
      get 'post_comments/:id', to: 'posts#post_comments'
      get 'types', to: 'types#index'

      resources :preferences, only: %i[show update]
      resources :item_preferences

      resources :posts, only: %i[index show create update destroy] do
        member do
          post 'like', to: 'likes#create'
          delete 'like', to: 'likes#destroy'
        end
      end

      resources :comments, only: %i[show create update destroy]

      resources :users, only: %i[show create update destroy] do
        member do
          get 'following'
          get 'followers'
          post 'follow', to: 'follows#create'
          delete 'follow', to: 'follows#destroy'
        end
        get 'current', on: :collection
      end
    end
  end
end
