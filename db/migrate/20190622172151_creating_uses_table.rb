class CreatingUsesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :username
      t.string :password_digest
      t.references :country
      t.references :state
      t.references :city

      t.timestamps
    end
  end
end
