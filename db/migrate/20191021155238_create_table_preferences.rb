class CreateTablePreferences < ActiveRecord::Migration[5.2]
  def change
    create_table :preferences do |t|
      t.belongs_to :user
      t.text :description

      t.timestamps
    end
  end
end
