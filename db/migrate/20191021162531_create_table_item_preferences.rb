class CreateTableItemPreferences < ActiveRecord::Migration[5.2]
  def change
    create_table :item_preferences do |t|
      t.belongs_to :preference
      t.belongs_to :type, optional: true
      t.belongs_to :city, optional: true
      t.belongs_to :state, optional: true
      t.belongs_to :country, optional: true

      t.timestamps
    end
  end
end
