class AddColumnsToPosts < ActiveRecord::Migration[5.2]
  def change
    add_reference :posts, :country
    add_reference :posts, :state
    add_reference :posts, :city
    add_reference :posts, :type
    add_column :posts, :photo, :string
  end
end
