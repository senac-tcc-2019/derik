Post.delete_all
Comment.delete_all
Preference.delete_all
ItemPreference.delete_all
User.delete_all
Type.delete_all
Country.delete_all
State.delete_all
City.delete_all

#Brasil
brasil = Country.create(name: 'Brasil')

#States from Brasil
rs = State.create(name: 'Rio Grande do Sul', country_id: brasil.id)
sc = State.create(name: 'Santa Catarina', country_id: brasil.id)
ba = State.create(name: 'Bahia', country_id: brasil.id)
al = State.create(name: 'Alagoas', country_id: brasil.id)
ce = State.create(name: 'Ceará', country_id: brasil.id)
go = State.create(name: 'Goiás', country_id: brasil.id)
ma = State.create(name: 'Maranhão', country_id: brasil.id)
mt = State.create(name: 'Mato Grosso', country_id: brasil.id)
mg = State.create(name: 'Minas Gerais', country_id: brasil.id)
pa = State.create(name: 'Pará', country_id: brasil.id)
pe = State.create(name: 'Pernambuco', country_id: brasil.id)
pr = State.create(name: 'Paraná', country_id: brasil.id)
rj = State.create(name: 'Rio de Janeiro', country_id: brasil.id)
to = State.create(name: 'Tocantins', country_id: brasil.id)



#Cities from Rio Grande do Sul
City.create(name: 'Pelotas', state_id: rs.id)
City.create(name: 'Rio Grande', state_id: rs.id)
City.create(name: 'Porto Alegre', state_id: rs.id)
City.create(name: 'Caxias', state_id: rs.id)
City.create(name: 'Gramado', state_id: rs.id)

#Cities from Santa Catarina
City.create(name: 'Florianópolis', state_id: sc.id)
City.create(name: 'Criciúma', state_id: sc.id)
City.create(name: 'Camburiú', state_id: sc.id)

#Cities from Bahia
City.create(name: 'Salvador', state_id: ba.id)
City.create(name: 'Porto Seguro', state_id: ba.id)
City.create(name: "Arraial D'ajuda", state_id: ba.id)

#cities from Alagoas
City.create(name: 'Maceió', state_id: al.id)

#cities from Ceará
City.create(name: 'Fortaleza', state_id: ce.id)

#cities from Curitiba
City.create(name: 'Curitiba', state_id: pr.id)

#cities from Goiás
City.create(name: 'Goiânia', state_id: go.id)

#cities from Maranhão
City.create(name: 'São Luis', state_id: ma.id)

#cities from Mato Grosso
City.create(name: 'Cuiabá', state_id: mt.id)

#cities from Minas Gerais
City.create(name: 'Belo Horizonte', state_id: mg.id)

#cities from Pará
City.create(name: 'Belém', state_id: pa.id)

#cities from Pernambuco
City.create(name: 'Recífe', state_id: pe.id)

#cities from Rio de Janeiro
City.create(name: 'Rio de Janeiro', state_id: rj.id)

#cities from Tocantins
City.create(name: 'Palmas', state_id: to.id)

#Estados Unidos
eua = Country.create(name: 'Estados Unidos')

#States from Estados Unidos
florida = State.create(name: 'Flórida', country_id: eua.id)
texas = State.create(name: 'Texas', country_id: eua.id)

#Cities from Florida
City.create(name: 'Miami', state_id: florida.id)
City.create(name: 'Orlando', state_id: florida.id)

#Cities from Texas
City.create(name: 'Dallas', state_id: texas.id)
City.create(name: 'Houston', state_id: texas.id)
City.create(name: 'Austin', state_id: texas.id)

#Create Types
Type.create(name: 'Cachoeira')
Type.create(name: 'Canyon')
Type.create(name: 'Praia')
Type.create(name: 'Serra')
Type.create(name: 'Trilha')
