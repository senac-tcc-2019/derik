FactoryBot.define do
  factory :city do
    name { FFaker::Lorem.phrase }
    state { State.first || create(:state) }
  end
end
