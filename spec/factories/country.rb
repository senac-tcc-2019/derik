FactoryBot.define do
  factory :country do
    name { FFaker::Lorem.phrase }
  end
end
