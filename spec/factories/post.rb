FactoryBot.define do
  factory :post do
    body { FFaker::Lorem.phrase }
    user
  end
end
