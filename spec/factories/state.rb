FactoryBot.define do
  factory :state do
    name { FFaker::Lorem.phrase }
    country { Country.first || create(:country) }
  end
end
