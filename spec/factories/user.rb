FactoryBot.define do
  factory :user do
    name { FFaker::Lorem.word }
    last_name { FFaker::Lorem.word }
    username { FFaker::Lorem.word }
    email { FFaker::Internet.email }
    password { 'secret123' }
    country_id { Country.first.id || create(:country).id }
    state_id { State.first.id || create(:state).id }
    city_id { City.first.id || create(:city).id }
  end
end
