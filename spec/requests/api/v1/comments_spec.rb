require 'rails_helper'

RSpec.describe "Api::V1::Comments", type: :request do
  describe 'GET /api/v1/comments/:id' do
    context 'when comment exists' do
      let(:user) { create(:user) }

      context 'regular comment' do
        let(:post) { create(:post) }
        let!(:comment) { create(:comment, body: 'testing comments' , post: post, user: user) }

        before { get "/api/v1/comments/#{comment.id}" }

        it { expect(response).to have_http_status(:success) }

        it 'returns valid post in json' do
          expect(json).to eql({ 'body' => comment.body, 'id' => comment.id, 'post_id' => post.id, 'user_id' => user.id})
        end

        it 'post owner is present' do
          expect(json['user_id']).to eql(user.id)
        end
      end
    end

    context 'when comment dont exist' do
      let(:comment_id) { -1 }

      before { get "/api/v1/comments/#{comment_id}" }

      it { expect(response).to have_http_status(:not_found) }
    end
  end

  describe 'POST /api/v1/comments' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      let(:user) { create(:user) }

      context 'Valid params' do
        context 'regular comment' do
          let(:posted) { create(:post) }
          let(:comment_params) { {user_id: user.id, post_id: posted.id, body: 'testing comment'} }

          it 'return created' do
            post '/api/v1/comments/', params: { comment: comment_params }, headers: header_with_authentication(user)
            expect(response).to have_http_status(:created)
          end

          it 'returns right comment in json' do
            post '/api/v1/comments/', params: { comment: comment_params }, headers: header_with_authentication(user)
            expect(json).to include_json(comment_params)
          end

          it 'create comment' do
            expect do
              post '/api/v1/comments/', params: { comment: comment_params }, headers: header_with_authentication(user)
            end.to change { Comment.count }.by(1)
          end
        end
      end

      context 'Invalid params' do
        let(:comment_params) { {foo: :bar} }

        before { post '/api/v1/comments/', params: { comment: comment_params }, headers: header_with_authentication(user) }

        it { expect(response).to have_http_status(:unprocessable_entity) }
      end
    end
  end

  describe 'DELETE /api/v1/comments/:comment_id' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      context 'Resource owner' do
        let(:user) { create(:user) }
        let(:posted) { create(:post) }
        let!(:comment) { create(:comment, post: posted, user: user, body: 'testing comment') }

        it do
          delete "/api/v1/comments/#{comment.id}", headers: header_with_authentication(user)
          expect(response).to have_http_status(:no_content)
        end

        it 'delete comment' do
          expect do
            delete "/api/v1/comments/#{comment.id}", headers: header_with_authentication(user)
          end.to change { Comment.count }.by(-1)
        end
      end

      context 'Not resource owner' do
        let(:user) { create(:user) }
        let(:other_user) { create(:user) }
        let(:posted) { create(:post) }
        let!(:comment) { create(:comment, post: posted, user: other_user, body: 'testing comment') }

        before do
          delete "/api/v1/comments/#{comment.id}", headers: header_with_authentication(user)
        end

        it { expect(response).to have_http_status(:forbidden) }
      end
    end
  end

  describe 'PUT /api/v1/comments/:comment_id' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      let(:user) { create(:user) }
      let(:posted) { create(:post) }
      let!(:comment) { create(:comment, user_id: user.id, post_id: posted.id, body: 'testing comment') }

      context 'Resource owner' do
        let(:comment_params) { { post_id: posted.id, body: 'updating comment' }}

        before { put "/api/v1/comments/#{comment.id}", params: { comment: comment_params }, headers: header_with_authentication(user) }

        it { expect(response).to have_http_status(:success) }

        it 'returns post updated in json' do
          put "/api/v1/comments/#{comment.id}", params: { comment: comment_params }, headers: header_with_authentication(user)
          expect(json).to include_json(comment_params)
        end
      end

      context 'Not resource owner' do
        let(:other_user) { create(:user) }
        let(:comment_params) { {post_id: posted.id, body: 'updating comment'} }

        before do
          put "/api/v1/comments/#{comment.id}", params: { comment: comment_params }, headers: header_with_authentication(other_user)
        end

        it { expect(response).to have_http_status(:forbidden) }
      end
    end
  end
end
