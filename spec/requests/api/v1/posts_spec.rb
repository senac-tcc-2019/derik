require 'rails_helper'

RSpec.describe "Api::V1::Posts", type: :request do
  describe 'GET /api/v1/posts?user_id=:id&page=:page' do
    context 'User exists' do
      let(:user) { create(:user) }
      let(:posts_number) { Random.rand(15..25) }

      before { posts_number.times { create(:post, user: user) } }

      it do
        get "/api/v1/posts?user_id=#{user.id}&page=1", headers: header_with_authentication(user)
        expect(response).to have_http_status(:success)
      end

      it 'returns right posts' do
        get "/api/v1/posts?user_id=#{user.id}&page=1", headers: header_with_authentication(user)
        expect(json).to eql(each_serialized(Api::V1::PostSerializer, user.posts[0..14]))
      end

      it 'returns 15 elemments on first page' do
        get "/api/v1/posts?user_id=#{user.id}&page=1", headers: header_with_authentication(user)
        expect(json.count).to eql(15)
      end

      it 'returns remaining elemments on second page' do
        get "/api/v1/posts?user_id=#{user.id}&page=2", headers: header_with_authentication(user)
        remaining = user.posts.count - 15
        expect(json.count).to eql(remaining)
      end
    end

    context 'User dont exist' do
      let(:user) { create(:user) }
      let(:user_id) { -1 }

      before { get "/api/v1/posts?user_id=#{user_id}&page=1", headers: header_with_authentication(user) }

      it { expect(response).to have_http_status(:not_found) }
    end
  end

  describe 'GET /api/v1/posts/:id' do
    context 'when post exists' do
      let(:user) { create(:user) }

      context 'regular post' do
        let(:post) { create(:post) }

        before { get "/api/v1/posts/#{post.id}" }

        it { expect(response).to have_http_status(:success) }

        it 'returns valid post in json' do
          expect(json).to eql(serialized(Api::V1::PostSerializer, post))
        end

        it 'post owner is present' do
          expect(json['user']).to eql(serialized(Api::V1::UserSerializer, post.user))
        end
      end
    end

    context 'when post dont exist' do
      let(:post_id) { -1 }

      before { get "/api/v1/posts/#{post_id}" }

      it { expect(response).to have_http_status(:not_found) }
    end
  end

  describe 'POST /api/v1/posts' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      let(:user) { create(:user) }

      context 'Valid params' do
        context 'regular post' do
          let(:post_params) { attributes_for(:post) }

          it 'return created' do
            post '/api/v1/posts/', params: { post: post_params }, headers: header_with_authentication(user)
            expect(response).to have_http_status(:created)
          end

          it 'returns right post in json' do
            post '/api/v1/posts/', params: { post: post_params }, headers: header_with_authentication(user)
            expect(json).to include_json(post_params)
          end

          it 'create post' do
            expect do
              post '/api/v1/posts/', params: { post: post_params }, headers: header_with_authentication(user)
            end.to change { Post.count }.by(1)
          end
        end
      end

      context 'Invalid params' do
        let(:post_params) { {foo: :bar} }

        before { post '/api/v1/posts/', params: { post: post_params }, headers: header_with_authentication(user) }

        it { expect(response).to have_http_status(:unprocessable_entity) }
      end
    end
  end

  describe 'DELETE /api/v1/posts/:post_id' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      context 'Resource owner' do
        let(:user) { create(:user) }
        before { @post = create(:post, user: user) }

        it do
          delete "/api/v1/posts/#{@post.id}", headers: header_with_authentication(user)
          expect(response).to have_http_status(:no_content)
        end

        it 'delete post' do
          expect do
            delete "/api/v1/posts/#{@post.id}", headers: header_with_authentication(user)
          end.to change { Post.count }.by(-1)
        end
      end

      context 'Not resource owner' do
        let(:user) { create(:user) }
        let(:other_user) { create(:user) }
        let(:post) { create(:post, user: other_user) }

        before do
          delete "/api/v1/posts/#{post.id}", headers: header_with_authentication(user)
        end

        it { expect(response).to have_http_status(:forbidden) }
      end
    end
  end

  describe 'PUT /api/v1/posts/:post_id' do
    context 'Unauthenticated' do
      it_behaves_like :deny_without_authorization, :put, '/api/v1/users/-1'
    end

    context 'Authenticated' do
      context 'Resource owner' do
        let(:user) { create(:user) }
        let(:post) { create(:post, user: user) }
        let(:post_params) { attributes_for(:post) }

        before { put "/api/v1/posts/#{post.id}", params: { post: post_params }, headers: header_with_authentication(user) }

        it { expect(response).to have_http_status(:success) }

        it 'returns post updated in json' do
          put "/api/v1/posts/#{post.id}", params: { post: post_params }, headers: header_with_authentication(user)
          expect(json).to include_json(post_params)
        end
      end

      context 'Not resource owner' do
        let(:user) { create(:user) }
        let(:other_user) { create(:user) }
        let(:post) { create(:post, user: other_user) }
        let(:post_params) { attributes_for(:post) }


        before do
          put "/api/v1/posts/#{post.id}", params: { post: post_params }, headers: header_with_authentication(user)
        end

        it { expect(response).to have_http_status(:forbidden) }
      end
    end
  end
end
